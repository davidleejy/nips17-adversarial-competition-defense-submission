Download the checkpoints for the neural network classifiers to be used:

- Ensemble Adversarially-trained Inception Resnet v2
- Adversarially-trained Inception V3
- Plain Inception V4
- Plain resnet v2

Cleverhans github has the chkpts for the adversarially trained NNs.

https://github.com/tensorflow/models/tree/master/research/slim has checkpoints for non adversarially trained ones.