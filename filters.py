# Filters Module
import numpy as np
# import scipy
from scipy.misc import imread, imsave, toimage
from scipy.ndimage.filters import median_filter, gaussian_filter
from scipy.ndimage import zoom, rotate


def run_median_filter(im, w, h):
    # im: hxwx3 RGB image.
    # w,h: width & height of filter.
    imf = np.empty(shape=im.shape, dtype=float)
    for d in range(3):
        imf[:,:,d] = median_filter(im[:,:,d], size=(h, w))
    return imf

def run_gaussian_filter(im, sigma):
    # im: hxwx3 RGB image.
    imf = np.empty(shape=im.shape, dtype=float)
    for d in range(3):
        imf[:,:,d] = gaussian_filter(im[:,:,d], sigma)
    # imf = np.clip(imf, 0.0, 255.0)
    return imf

def crop_center(img, cropx, cropy):
    y,x = img.shape[0:2]
    startx = x//2-(cropx//2)
    starty = y//2-(cropy//2)    
    return img[starty:starty+cropy, startx:startx+cropx]

def checkimrange_fail(im):
  if np.any(im<0) or np.any(im>255):
    return True
  else:
    return False
    
def run_zoom(im, factor, resize=None, padding_val=0):
    # im: hxwx3 RGB image. WARNING: REQUIRES 0~255 intensities.
    # resize: (h, w) of final image.
    imz = zoom(im, (factor, factor, 1))
    # imz = np.clip(imz, 0.0, 255.0)
    #if checkimrange_fail(imz): print('checkfialed in zoom')
    # print('zoom', imz.shape)
    if not resize is None:
      if factor > 1 :
        return crop_center(imz, cropx=resize[1], cropy=resize[0])
      else : # pad instead
        cur_y = imz.shape[0]; cur_x = imz.shape[1]
        total_pad_x = resize[1] - cur_x
        total_pad_y = resize[0] - cur_y
        pad_amt_bef_x = total_pad_x // 2
        pad_amt_bef_y = total_pad_y // 2
        return np.pad(imz, 
                      [(pad_amt_bef_y, total_pad_y - pad_amt_bef_y), (pad_amt_bef_x, total_pad_x - pad_amt_bef_x), (0,0)], mode='constant', constant_values=padding_val)
    else:
        return imz

def run_rotate(im, angle_deg):
    # im: hxwx3 RGB image. WARNING: REQUIRES 0~255 intensities.
    imr = np.empty(shape=im.shape, dtype=float)
    rotate(im, angle_deg, reshape=False, output=imr)
    # imr = np.clip(imr, 0.0, 255.0)
    return imr

if __name__ == "__main__":
    f="/data/d/aadef/dataset_nonsense/images/copy1.pngabc"
    image = imread(f, mode='RGB').astype(np.float) / 255.0

    im_mf = run_median_filter(image, w=1, h=5)
    toimage(im_mf, cmin=0, cmax=1).save('im_mf.png')
    
    im_gf = run_gaussian_filter(image, 0.8)
    toimage(im_gf, cmin=0, cmax=1).save('im_gf.png')
    

    f="/data/d/aadef/dataset_nonsense/images/copy1.pngabc"
    image = imread(f, mode='RGB').astype(np.float) #intensity 0~255.

    im_z = run_zoom(image, 1.20, resize=(image.shape[0], image.shape[1]))
    toimage(im_z, cmin=0, cmax=255).save('im_z.png')
    
    im_r = run_rotate(image, -11)
    toimage(im_r, cmin=0, cmax=255).save('im_r.png')
    

    # Trying out a combination of transformations:
    im_ct = run_median_filter(image, w=1, h=5)
    im_ct = run_gaussian_filter(im_ct, 0.48)
    im_ct = run_zoom(im_ct, 1.018, resize=(image.shape[0], image.shape[1]))
    im_ct = run_rotate(im_ct, -6.89)
    toimage(im_ct, cmin=0, cmax=255).save('im_ct.png')
    # imsave('im_ct.png', im_ct.astype(np.uint8) )    

    # imsave('im_z.png', (im_z*255.0).astype(np.uint8) )

    # im_z2 = zoom(image,(1.2,1.2,1))
    # print(im_z2.shape)
    # imsave('im_z2.png', (im_z2*255.0).astype(np.uint8) )

    # image = image[0: orig_wt, 0: orig_ht]



    # imfv1 = run_median_filter(image, 1,1)
    # # filt = np.ones(4,4,3)
    # # filt[]
    # # image = median_filter(image, size=(5, 1, 3)) #5x1xrgb filter.
    # image1 = median_filter(image, size=1) #5x1xrgb filter.
    # image2 = median_filter(image, size=(1, 1, 3)) #5x1xrgb filter.
    # print(np.all(np.abs(image1-image2)<1e-6))
    # print(np.all(np.abs(image-image2)<1e-6))
    # print(np.all(np.abs(imfv1-image1)<1e-6))
    # imsave('asdf1.png', (image1*255.0).astype(np.uint8) )
    # imsave('asdf2.png', (image2*255.0).astype(np.uint8) )
    # imsave('imfv1.png', (imfv1*255.0).astype(np.uint8) )


    # imgf1 = gaussian_filter(image,2)
    # imgf2 = run_gaussian_filter(image,2)
    # print(np.all(np.abs(imgf1-imgf2)<1e-6))
    # imsave('imgf1.png', (imgf1*255.0).astype(np.uint8) )
    # imsave('imgf2.png', (imgf2*255.0).astype(np.uint8) )

    # immf1x299 = run_median_filter(image, w=1,h=299)
    # imsave('immf1x299.png', (immf1x299 * 255.0).astype(np.uint8) )

    # immf5x50 = run_median_filter(image, w=5,h=50)
    # imsave('immf5x50.png', (immf5x50 * 255.0).astype(np.uint8) )
