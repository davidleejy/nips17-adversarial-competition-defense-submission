import os
import tensorflow as tf
#import keras.backend.tensorflow_backend as KTF

def set_cuda_visible_devices(indices):
    # e.g. indices == '0' or '1' or '2' ...
    os.environ["CUDA_VISIBLE_DEVICES"]=indices #indexed 0,1,2..
    
def get_cuda_visible_devices():
    return os.environ.get('CUDA_VISIBLE_DEVICES')

def get_session(gpu_fraction=0.1):
    '''Assume that you have 6GB of GPU memory and want to allocate ~0.6GB'''
    num_threads = os.environ.get('OMP_NUM_THREADS')
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=gpu_fraction)
    if num_threads:
        return tf.Session(config=tf.ConfigProto(
            gpu_options=gpu_options, intra_op_parallelism_threads=num_threads))
    else:
        return tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))

# Example of starting