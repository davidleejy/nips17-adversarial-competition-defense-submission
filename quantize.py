# Feature Squeeze Random Quantization.
import numpy as np
# import scipy
# from scipy.misc import imread, imsave

def midpoints(bounds):
  mids = np.zeros(len(bounds) - 1)
  for i in range(len(bounds)- 1):
    mids[i] = bounds[i] + ((bounds[i+1] - bounds[i]) / 2)
  return mids

def quantized_im(bin_alloc, bins):
    q_im = np.empty(shape=bin_alloc.shape, dtype=float)
    for i in range(q_im.shape[0]):
        for j in range(q_im.shape[1]):
            for k in range(q_im.shape[2]):
                q_im[i, j, k] = bins[bin_alloc[i, j, k] - 1]
    return q_im

def run(im, min, max, quant_bins, quant_rand_delta):
  # Create bins:
  bins = np.arange(min, max + 1e-10, 1.0 * (max - min) / quant_bins)
  # Impute randomness into bin boundaries:
  deltas = np.random.uniform(-quant_rand_delta, quant_rand_delta, quant_bins - 1)
  for i in range(1, quant_bins):
    bins[i] = bins[i] + deltas[i - 1]
  #print(bins)
  # Bin the intensities:
  bin_alloc = np.digitize(im, bins)
  # Find bins' midpoints:
  mids = midpoints(bins)
  return quantized_im(bin_alloc, bins)

# if __name__ == "__main__":
  # quant_bins = 20
  # quant_rand_delta = 1.0 / 255 * 2
  # f="/data/d/aadef/dataset_nonsense/images/copy1.png"
  # image = imread(f, mode='RGB').astype(np.float) / 255.0
  # q_im = run(image, 0, 1, quant_bins, quant_rand_delta)
  # imsave('asdf.png', (q_im*255.0).astype(np.uint8) )