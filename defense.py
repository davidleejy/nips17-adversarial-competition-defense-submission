from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import time
import sys
import os
import inception_resnet_v2 #EAIRV2
import numpy as np
import scipy
from scipy.misc import imread

import tensorflow as tf
from tensorflow.contrib.slim.nets import inception #EAIRV2 #AIV3
# from tensorflow.contrib.slim.nets import inception
from nets import inception as inception_extra #IV4
from nets import inception_utils as inception_utils_extra #IV4
from nets import resnet_utils as resnet_utils_extra #RV2
from nets import resnet_v2 as resnet_v2_extra #RV2

from k_dav import set_cuda_visible_devices
import quantize
import filters
# from scipy.ndimage.filters import gaussian_filter

slim = tf.contrib.slim

# tf.flags.DEFINE_string(
    # 'master', '', 'The address of the TensorFlow master to use.')

# tf.flags.DEFINE_string(
    # 'checkpoint_path', '', 'Path to checkpoint for inception network.')

tf.flags.DEFINE_string(
    'input_dir', '', 'Input directory with images.')

tf.flags.DEFINE_string(
    'output_file', '', 'Output file to save labels.')

tf.flags.DEFINE_integer(
    'image_width', 299, 'Width of each input images.')

tf.flags.DEFINE_integer(
    'image_height', 299, 'Height of each input images.')

tf.flags.DEFINE_integer(
    'batch_size', 16, 'How many images process at one time.')

tf.flags.DEFINE_integer(
  'gpu_mem_aggressive', 1, 'Set 0 to use slow_growth etc. . Default (1) aggressive.')

tf.flags.DEFINE_string(
    'visible_gpus', '', 'E.g. 0,1 means use GPUs idxed 0 and 1.')

# tf.flags.DEFINE_integer(
    # 'quant_bins', 255, 'Feat Sqz. Quantizatn. Default (255) none.')

# tf.flags.DEFINE_float(
    # 'quant_rand_delta', 0, 'Feat Sqz. Rand bin-boundary quantizatn. E.g. If set to 1/255, then a bin''s boundary can waver by +- 1/255 from usual boundary. Default (0) none.')

# tf.flags.DEFINE_float(
  # 'gaussian_filter_sigma', 0, 'Feat Sqz gaussian filter. Default (0) none.')

# tf.flags.DEFINE_integer(
  # 'median_filter_w', 1, 'Feat Sqz median filter width. Default (1) none.')

# tf.flags.DEFINE_integer(
  # 'median_filter_ht', 1, 'Feat Sqz median filter height. Default (1) none.')

# tf.flags.DEFINE_float(
  # 'zoom', 1.0, 'Feat Sqz zoom factor.Should be >=1. Default (1) none.')

# tf.flags.DEFINE_float(
  # 'rot_angle_deg', 0.0, 'Rotate angle (degs). Default (0) none.')  
  
FLAGS = tf.flags.FLAGS

# def checkimrange_fail(im):
  # if np.any(im<0) or np.any(im>255):
    # return True
  # else:
    # return False
    

def load_images(input_dir, batch_shape):
  """Read png images from input directory in batches.

  Args:
    input_dir: input directory
    batch_shape: shape of minibatch array, i.e. [batch_size, height, width, 3]

  Yields:
    filenames: list file names without path of each image
      Lenght of this list could be less than batch_size, in this case only
      first few images of the result are elements of the minibatch.
    images: array with all images from this batch
  """
  n_transforms = 16 #20 seems ok too.
  images = np.zeros([n_transforms] + batch_shape)
  filenames = []
  idx = 0
  batch_size = batch_shape[0]
  for filepath in tf.gfile.Glob(os.path.join(input_dir, '*.png')):
    with tf.gfile.Open(filepath) as f:
      image_orig = imread(f, mode='RGB').astype(np.float) #0~255 intensity
      
      for t_idx in range(n_transforms):
        mf_w = np.random.randint(1,3) #unif distrib. range [1, 3).
        mf_h = np.random.randint(1,6) #unif distrib. range [1, 6).
        gf_sigma = np.random.uniform(0.02, 0.4) # range [low,hi)
        zfactor = np.random.uniform(0.98, 1.03) #prev 1.018
        rot_ang_deg= np.random.uniform(-45.0, 45.0) # prev -6.89
        quant_bins = np.random.randint(9, 256) # range [8, 256)#prev 17
        quant_rand_delta = 255. / quant_bins * np.random.uniform(0.05, 0.15) # prev 2
        # print('^'*5, 't_idx', t_idx,
              # 'mf_w' ,mf_w, 'mf_h' ,mf_h,
              # 'gf_sigma', gf_sigma,
              # 'zfactor', zfactor,
              # 'rot_ang_deg', rot_ang_deg,
              # 'quant_bins', quant_bins,
              # 'quant_rand_delta', quant_rand_delta)
      
        # Median Filter:
        image = filters.run_median_filter(image_orig, mf_w, mf_h)
        # if checkimrange_fail(image): print('mf t_idx', t_idx, 'FAIL' *10) #todo remove
        # Gaussian Filter:
        image = filters.run_gaussian_filter(image, gf_sigma)
        # if checkimrange_fail(image): print('gf t_idx', t_idx, 'FAIL' *10) #todo remove
        # Zoom:
        image = filters.run_zoom(image, zfactor, 
          resize=(FLAGS.image_height, FLAGS.image_width))
        # if checkimrange_fail(image): print('zz t_idx', t_idx, 'FAIL' *10) #todo remove
        # Rotate:
        image = filters.run_rotate(image, rot_ang_deg)
        # if checkimrange_fail(image): print('rot t_idx', t_idx, 'FAIL' *10) #todo remove
        # Feat Squeeze Quantization:
        image = np.clip(image, 0.0, 255.0)
        if np.random.randint(100) < 25:
          image = quantize.run(image, min=0.0, max=255.0, 
            quant_bins=quant_bins, 
            quant_rand_delta=quant_rand_delta)
        
        # print('Test whether defaults really didn''t change image.')
        # assert(np.all(np.abs(image_orig - image) < 1e-6))
        
        # Save transformed images and eyeball:
        # timname = 'b' + str(idx) + 't' + str(t_idx)
        # timpath = (FLAGS.output_file).replace('result.csv',timname + '.png')
        # scipy.misc.toimage(image, cmin=0.0, cmax=255.0).save(timpath) 
        
        # Images for inception classifier are normalized to be in [-1, 1] interval.
        # print('image.shape', image.shape)
        images[t_idx, idx, :, :, :] = image.copy() / 255.0 * 2.0 - 1.0
    filenames.append(os.path.basename(filepath))
    idx += 1
    if idx == batch_size:
      yield filenames, images
      filenames = []
      images = np.zeros([n_transforms] + batch_shape)
      idx = 0
  if idx > 0:
    yield filenames, images

def save_sth(sth, filepath):
  '''Args:
    sth: string
    filepath: path to file.
  '''
  with tf.gfile.Open(filepath, 'w') as f:
    f.write(sth)
  f.close()

def main(_):

  print('FLAGS.batch_size', FLAGS.batch_size)
  print('FLAGS.gpu_mem_aggressive', FLAGS.gpu_mem_aggressive)
  print('FLAGS.visible_gpus', FLAGS.visible_gpus)
  # print('FLAGS.quant_bins', FLAGS.quant_bins)
  # print('FLAGS.quant_rand_delta', FLAGS.quant_rand_delta)
  # print('FLAGS.gaussian_filter_sigma', FLAGS.gaussian_filter_sigma)
  # print('FLAGS.median_filter_w', FLAGS.median_filter_w)
  # print('FLAGS.median_filter_ht', FLAGS.median_filter_ht)
  # print('FLAGS.zoom', FLAGS.zoom)
  # print('FLAGS.rot_angle_deg', FLAGS.rot_angle_deg)

  batch_shape = [FLAGS.batch_size, FLAGS.image_height, FLAGS.image_width, 3]
  num_classes = 1001
  
  config_tf = tf.ConfigProto();
  if 0==FLAGS.gpu_mem_aggressive:
    config_tf.allow_soft_placement = True
    config_tf.gpu_options.allow_growth=True
    config_tf.gpu_options.visible_device_list = FLAGS.visible_gpus
  sess = tf.Session(config=config_tf)
  #tf.logging.set_verbosity(tf.logging.INFO)
  if 0==FLAGS.gpu_mem_aggressive:
    time1 = time.time()
  # with tf.Graph().as_default():
  
  x_input = tf.placeholder(tf.float32, shape=batch_shape)
  
  # def printdict(d):
    # for i in d:
      # print(i, d[i])
  
  # def printnearbyprobs(pred_cls, probs):
    # i = 0; L=[]
    # for p in pred_cls:
      # L += [probs[i, p-2:p+2]]
      # i +=1
    # print(L)
  
  # EAIRV2  ---------------
  with slim.arg_scope(inception_resnet_v2.inception_resnet_v2_arg_scope()):
    logits_EAIRV2, end_points_EAIRV2 = inception_resnet_v2.inception_resnet_v2(
        x_input, num_classes=1001, is_training=False)
  #output_EAIRV2 = end_points_EAIRV2['Predictions']
  pred_cls_EAIRV2 = tf.argmax(end_points_EAIRV2['Predictions'], 1)
  probs_EAIRV2 = tf.nn.softmax(logits_EAIRV2)
  #probs_EAIRV2 = output_EAIRV2.op.inputs[0]
  restore_vars = [v for v in tf.global_variables() if v.name.startswith('InceptionResnetV2/')]
  saver = tf.train.Saver(restore_vars)
  saver.restore(sess, 'ens_adv_inception_resnet_v2.ckpt')
  # ----------------------------
  # AIV3  ---------------
  with slim.arg_scope(inception.inception_v3_arg_scope()):
    logits_AIV3, end_points_AIV3 = inception.inception_v3(
        x_input, num_classes=1001, is_training=False)
  # output_AIV3 = end_points_AIV3['Predictions']
  pred_cls_AIV3 = tf.argmax(end_points_AIV3['Predictions'], 1)
  probs_AIV3 = tf.nn.softmax(logits_AIV3)
  restore_vars = [v for v in tf.global_variables() if v.name.startswith('InceptionV3/')]
  saver = tf.train.Saver(restore_vars)
  saver.restore(sess, 'adv_inception_v3.ckpt')
  # ----------------------------
  # IV4  ---------------
  with slim.arg_scope(inception_utils_extra.inception_arg_scope()):
    logits_IV4, end_points_IV4 = inception_extra.inception_v4(
        x_input, num_classes=1001, is_training=False)
  #output_IV4 = end_points_IV4['Predictions']
  pred_cls_IV4 = tf.argmax(end_points_IV4['Predictions'], 1)
  probs_IV4 = tf.nn.softmax(logits_IV4)
  # printdict(end_points_IV4)
  # print('^^'*30)
  restore_vars = [v for v in tf.global_variables() if v.name.startswith('InceptionV4/')]
  saver = tf.train.Saver(restore_vars)
  saver.restore(sess, 'inception_v4.ckpt')
  # ----------------------------
  # RV2  ---------------
  with slim.arg_scope(resnet_utils_extra.resnet_arg_scope()):
    logits_RV2, end_points_RV2 = resnet_v2_extra.resnet_v2_152(
        x_input, num_classes=1001, is_training=False)
  pred_cls_RV2 = tf.argmax(end_points_RV2['predictions'], 1)
  probs_RV2 = tf.nn.softmax(logits_RV2)
  # printdict(end_points_RV2)
  # print('^^'*30)
  restore_vars = [v for v in tf.global_variables() if v.name.startswith('resnet_v2_152/')]
  saver = tf.train.Saver(restore_vars)
  saver.restore(sess, 'resnet_v2_152.ckpt')
  # ----------------------------
  # IV3  ---------------
  # Plain IV3 not used.
  # ----------------------
  
  # Run computation
  # saver = tf.train.Saver(slim.get_model_variables())
  # config_tf = tf.ConfigProto();
  # if 0==FLAGS.gpu_mem_aggressive:
    # config_tf.allow_soft_placement = True
    # config_tf.gpu_options.allow_growth=True
    # config_tf.gpu_options.visible_device_list = FLAGS.visible_gpus
  # saver = tf.train.Saver(slim.get_model_variables())
  # session_creator = tf.train.ChiefSessionCreator(
      # scaffold=tf.train.Scaffold(saver=saver),
      # checkpoint_filename_with_path=FLAGS.checkpoint_path,
      # master=FLAGS.master,
      # config=config_tf)

  # with tf.train.MonitoredSession(session_creator=session_creator) as sess:
    # with tf.gfile.Open(FLAGS.output_file, 'w') as out_file:
      # for filenames, images in load_images(FLAGS.input_dir, batch_shape):
        # labels = sess.run(predicted_labels, feed_dict={x_input: images})
        # for filename, label in zip(filenames, labels):
          # out_file.write('{0},{1}\n'.format(filename, label))
  if 0==FLAGS.gpu_mem_aggressive:
    cum_proced_ims = 0
  with tf.gfile.Open(FLAGS.output_file, 'w') as out_file:
    for filenames, images in load_images(FLAGS.input_dir, batch_shape):
      # filenames is len batch_size. images is shape [t_idx, batch_idx, 299, 299, 3]
      n_transforms = images.shape[0]
      probs_all_transforms = np.zeros([n_transforms, FLAGS.batch_size, 1001])
      for t_idx in range(n_transforms):
        pred_cls_np_EAIRV2, probs_np_EAIRV2 = sess.run([pred_cls_EAIRV2, probs_EAIRV2], feed_dict={x_input: images[t_idx,:,:,:,:]})
        # print('*'*10, 'pred_cls_np_EAIRV2', pred_cls_np_EAIRV2)
        # printnearbyprobs(pred_cls_np_EAIRV2, probs_np_EAIRV2)
        # print('^'*10, 'sum', np.sum(probs_np_EAIRV2, axis=1))
        
        pred_cls_np_AIV3, probs_np_AIV3 = sess.run([pred_cls_AIV3, probs_AIV3], feed_dict={x_input: images[t_idx,:,:,:,:]})
        # print('*'*10, 'pred_cls_np_AIV3', pred_cls_np_AIV3)
        # printnearbyprobs(pred_cls_np_AIV3, probs_np_AIV3)
        # print('^'*10, 'sum', np.sum(probs_np_AIV3, axis=1))
        
        pred_cls_np_IV4, probs_np_IV4 = sess.run([pred_cls_IV4, probs_IV4], feed_dict={x_input: images[t_idx,:,:,:,:]})
        # print('*'*10, 'pred_cls_np_IV4', pred_cls_np_IV4)
        # printnearbyprobs(pred_cls_np_IV4, probs_np_IV4)
        # print('^'*10, 'sum', np.sum(probs_np_IV4, axis=1))
        
        pred_cls_np_RV2, probs_np_RV2 = sess.run([pred_cls_RV2, probs_RV2], feed_dict={x_input: images[t_idx,:,:,:,:]})
        # print('*'*10, 'pred_cls_np_RV2', pred_cls_np_RV2)
        # printnearbyprobs(pred_cls_np_RV2, probs_np_RV2)
        # print('^'*10, 'sum', np.sum(probs_np_RV2, axis=1))
        
        ave_probs_1transform = (probs_np_EAIRV2 + probs_np_AIV3 + probs_np_IV4 + probs_np_RV2) / 4.
        # shape is (batch, 1001)
        # print('^'*10, 'sum ave_probs_1transform', np.sum(ave_probs_1transform, axis=1)) # can remove
        probs_all_transforms[t_idx,:,:] = ave_probs_1transform.copy()
      
      ave_probs = np.sum(probs_all_transforms, axis=0) / (n_transforms * 1.0)
      # print('^'*10, 'sum ave_probs', np.sum(ave_probs, axis=1)) # can remove
      # print('^'*10, 'ave_probs.shape', ave_probs.shape)
      # ave_probs.shape (batch, 1001)
      # print('^'*10, 'ave_probs', ave_probs)
      ave_preds = np.argmax(ave_probs, axis=1)
      # print('^'*10, 'ave_probs', ave_preds)
      
      # elapsed time
      if 0==FLAGS.gpu_mem_aggressive:
        time3 = time.time()
        try:
          time_taken_per_batch_s = str((time3 - time2))
        except:
          time_taken_per_batch_s = -1
          pass
        time2 = time.time()
        time_taken_s = str((time2 - time1))
        cum_proced_ims += len(filenames)
        print('cum_proced_ims', cum_proced_ims, 'elapsed secs', time_taken_s, 'time between batches secs', time_taken_per_batch_s); sys.stdout.flush()
      
      for filename, label in zip(filenames, ave_preds):
        out_file.write('{0},{1}\n'.format(filename, label))
  
  # end of with grpah as deaut...
  if 0==FLAGS.gpu_mem_aggressive:
    time2 = time.time()
    time_taken_mins = str((time2 - time1) / 60)
    save_sth(time_taken_mins + ' mins', (FLAGS.output_file).replace('csv','time'))

if __name__ == '__main__':
  tf.app.run()
