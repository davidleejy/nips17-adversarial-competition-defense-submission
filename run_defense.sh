#!/bin/bash
#
# run_defense.sh is a script which executes the defense
#
# Envoronment which runs attacks and defences calls it in a following way:
#   run_defense.sh INPUT_DIR OUTPUT_FILE
# where:
#   INPUT_DIR - directory with input PNG images
#   OUTPUT_FILE - file to store classification labels
#

INPUT_DIR=$1
OUTPUT_FILE=$2
USE_EXTRA_ARGS=$3
VISIBLE_GPUS=$4 #e.g. 0,1,2

echo "USE_EXTRA_ARGS: ${USE_EXTRA_ARGS}"

if [ "$USE_EXTRA_ARGS" = true ] ; then
    echo '>>>>>>> ue extra args'
    python defense.py \
      --input_dir="${INPUT_DIR}" \
      --output_file="${OUTPUT_FILE}" \
      --gpu_mem_aggressive=0 \
      --visible_gpus="${VISIBLE_GPUS}" \
      --batch_size=10
else #   FOR    COMPETITION   :
    echo '>>>>>>> COMPETION'
    python defense.py \
      --input_dir="${INPUT_DIR}" \
      --output_file="${OUTPUT_FILE}" \
      --batch_size=10
fi
